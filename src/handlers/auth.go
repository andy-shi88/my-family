package handlers

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/andy-shi88/my-family/src/models"
	customModel "gitlab.com/andy-shi88/my-family/src/models/custom_model"
	"gitlab.com/andy-shi88/my-family/src/services"
	apiutils "gitlab.com/andy-shi88/my-family/src/utils/api"
)

type AuthHandler struct {
	Service services.AuthService
}

func NewAuthHandler(service services.AuthService) *AuthHandler {
	return &AuthHandler{Service: service}
}

func (h AuthHandler) Handler(router *gin.Engine) {
	routes := router.Group("/auth")
	{
		routes.POST("/register", h.registerUser)
		routes.POST("/login", h.loginUser)
		routes.POST("/refreshToken", h.refreshToken)
	}
}

func (h AuthHandler) registerUser(c *gin.Context) {
	var user models.User
	if c.ShouldBindJSON(&user) == nil {
		data, err := h.Service.Register(user)
		if err != nil {
			response := apiutils.ResponseBuilder{}.SetMessage(err.Error()).SetSuccess(false).Build()
			c.JSON(http.StatusNotFound, response)
			return
		}
		response := apiutils.ResponseBuilder{}.SetData(data).SetMessage("user registered").SetSuccess(true).Build()
		c.JSON(http.StatusOK, response)
	} else {
		c.JSON(http.StatusBadRequest, apiutils.ResponseBuilder{}.SetMessage("wrong payload to register user").SetSuccess(false).Build())
	}
}

func (h AuthHandler) loginUser(c *gin.Context) {
	log.Println(c)
	var user models.User
	if c.ShouldBindJSON(&user) == nil {
		data, err := h.Service.Login(user)
		if err != nil {
			response := apiutils.ResponseBuilder{}.SetMessage(err.Error()).SetSuccess(false).Build()
			c.JSON(http.StatusNotFound, response)
			return
		}
		response := apiutils.ResponseBuilder{}.SetData(data).SetMessage("user logged in").SetSuccess(true).Build()
		c.JSON(http.StatusOK, response)
	} else {
		c.JSON(http.StatusBadRequest, apiutils.ResponseBuilder{}.SetMessage("wrong payload to register user").SetSuccess(false).Build())
	}
}

func (h AuthHandler) refreshToken(c *gin.Context) {
	var refresh customModel.RefreshRequest
	if c.ShouldBindJSON(&refresh) == nil {
		data, err := h.Service.RefreshToken(refresh)
		if err != nil {
			response := apiutils.ResponseBuilder{}.SetMessage(err.Error()).SetSuccess(false).Build()
			c.JSON(http.StatusNotFound, response)
			return
		}
		response := apiutils.ResponseBuilder{}.SetData(data).SetMessage("token refreshed").SetSuccess(true).Build()
		c.JSON(http.StatusOK, response)
	} else {
		c.JSON(http.StatusBadRequest, apiutils.ResponseBuilder{}.SetMessage("wrong payload to refresh token").SetSuccess(false).Build())

	}
}
