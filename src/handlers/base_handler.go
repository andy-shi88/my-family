package handlers

import (
	"github.com/gin-gonic/gin"
	apiutils "gitlab.com/andy-shi88/my-family/src/utils/api"
)

type BaseHandler func(*gin.Context) (int, error)

func (bh BaseHandler) ServeHTTP(c *gin.Context) {
	if status, err := bh(c); err != nil {
		response := apiutils.ResponseBuilder{}.SetMessage(err.Error()).SetSuccess(false).Build()
		c.JSON(status, response)
	}
}
