package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/andy-shi88/my-family/src/services"
	apiutils "gitlab.com/andy-shi88/my-family/src/utils/api"
)

type UserHandler struct {
	Service services.UserService
}

func NewUserHandler(service services.UserService) *UserHandler {
	return &UserHandler{Service: service}
}

func (u UserHandler) Handler(router *gin.Engine) {
	routes := router.Group("/users")
	{
		routes.GET("/", u.getUserList)
	}
}

func (u UserHandler) getUserList(c *gin.Context) {
	data, err := u.Service.GetUserList()
	if err != nil {
		response := apiutils.ResponseBuilder{}.SetMessage(err.Error()).SetSuccess(false).Build()
		c.JSON(http.StatusNotFound, response)
		return
	}
	response := apiutils.ResponseBuilder{}.SetData(data).SetMessage("user registered").SetSuccess(true).Build()
	c.JSON(http.StatusOK, response)
}
