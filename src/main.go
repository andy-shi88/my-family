package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
	handlers "gitlab.com/andy-shi88/my-family/src/handlers"
	database "gitlab.com/andy-shi88/my-family/src/modules/db"
	"gitlab.com/andy-shi88/my-family/src/repositories"
	"gitlab.com/andy-shi88/my-family/src/services"
)

func main() {
	// load env
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error loading .env file: %s", err)
	}
	// initialize database
	database.InitDb()
	defer database.CloseDb()
	// repository init
	log.Println("initializing repositories...")
	userRepository := repositories.NewUserRepository(database.Db)
	tokenRepository := repositories.NewTokenRepository(database.Db)
	// services init
	log.Println("initializing services...")
	authService := services.AuthService{Repository: userRepository, TokenRepository: tokenRepository}
	userService := services.UserService{Repository: userRepository}
	// initialize handlers
	log.Println("initializing handlers...")
	authHandler := handlers.NewAuthHandler(authService)
	userHandler := handlers.NewUserHandler(userService)
	// test middleware

	// initialize routes
	r := gin.Default()
	authHandler.Handler(r)
	userHandler.Handler(r)
	r.Run("127.0.0.1:" + os.Getenv("APP_PORT"))
}
