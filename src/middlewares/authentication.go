package middlewares

import (
	crypto "gitlab.com/andy-shi88/my-family/src/utils/crypto"
)

// Authenticate user token, will return token claim and error = nil
// if correct token passed
func AuthenticateUser(token string) (interface{}, error) {
	claim, err := crypto.VerifyJWT(token)
	if err != nil {
		return nil, err
	}
	return claim, nil
}
