package jwt_model

type JWTPayload struct {
	Username string `json:"username"`
	ID       uint   `json:"id"`
}
