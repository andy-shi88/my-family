package models

import (
	"time"
)

type Token struct {
	ID        uint       `json:"id"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`

	RefreshToken string `gorm:"size:120" json:"refresh_token"`
	User         User
	UserId       uint `json:"user_id"`
}

type TokenUser struct {
	ID        uint       `json:"id"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`

	RefreshToken string `gorm:"size:120" json:"refresh_token"`
	UserId       uint   `json:"user_id"`
	Username     string `gorm:"size:120" json:"username"`
}

func (p Token) TableName() string {
	return "access_tokens"
}

func (p TokenUser) TableName() string {
	return "access_tokens"
}
