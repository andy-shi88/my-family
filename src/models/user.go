package models

import (
	"time"
)

type User struct {
	ID        uint       `json:"id"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at"`

	Username string `gorm:"size:120" json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Salt     string `json:"salt"`
}

func (p User) TableName() string {
	return "users"
}
