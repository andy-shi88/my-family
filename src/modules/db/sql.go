package db

import (
	"log"
	"os"
	"strconv"

	"github.com/jinzhu/gorm"
)

var Db *gorm.DB

func InitDb() {
	log.Println("connecting to database")
	var err error
	Db, err = gorm.Open("mysql", os.Getenv("DB_URL"))
	gormLog, _ := strconv.ParseBool(os.Getenv("GORM_LOG"))
	Db.LogMode(gormLog)
	if err != nil {
		log.Panicf("fail to connect to database, error: %s", err)
	}
}

func CloseDb() {
	Db.Close()
}
