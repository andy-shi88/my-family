package repositories

import (
	"gitlab.com/andy-shi88/my-family/src/models"
)

type Repository interface {
	// should have returned []models.Model | but somehow it cannot be done
	Browse() ([]models.Model, error)
	Read(uint) (models.Model, error)
	Add(models.Model) (models.Model, error)
}
