package repositories

import (
	"log"

	"github.com/jinzhu/gorm"
	"gitlab.com/andy-shi88/my-family/src/models"
)

type TokenRepository struct {
	Db *gorm.DB
}

func NewTokenRepository(db *gorm.DB) *TokenRepository {
	return &TokenRepository{
		Db: db,
	}
}

func (tr TokenRepository) Browse() (interface{}, error) {
	log.Println("repositories:TokenRepository - Browse")
	result := []models.Token{}
	// fetch data
	tr.Db.Find(&result)
	log.Println("result: ", result)
	return result, nil
}

func (tr TokenRepository) Read(id uint) (models.Model, error) {
	log.Println("repositories:TokenRepository - Read")
	var token models.Token
	errs := tr.Db.Where("id = ?", id).First(&token).GetErrors()
	if len(errs) > 0 {
		log.Println("error occured:", errs[0])
		return nil, errs[0]
	}
	return token, nil
}

func (tr TokenRepository) Add(payload models.Model) (models.Model, error) {
	log.Println("repositories:TokenRepository - Create")
	token, _ := payload.(models.Token)
	errs := tr.Db.Create(&token).GetErrors()
	if len(errs) > 0 {
		log.Println("error occured:", errs[0])
		return nil, errs[0]
	}
	return token, nil
}

// extra implementation
func (tr TokenRepository) ReadByRefreshToken(refreshToken string) (models.Model, error) {
	log.Println("repositories:TokenRepository - ReadByRefreshToken")
	var token models.TokenUser
	errs := tr.Db.Where("refresh_token = ?", refreshToken).Select("access_tokens.*, users.username").Joins("JOIN users ON users.id = access_tokens.user_id").First(&token).GetErrors()
	if len(errs) > 0 {
		log.Println("error occured:", errs[0])
		return nil, errs[0]
	}
	return token, nil
}
