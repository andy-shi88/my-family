package repositories

import (
	"log"

	"github.com/jinzhu/gorm"
	"gitlab.com/andy-shi88/my-family/src/models"
)

type UserRepository struct {
	Db *gorm.DB
}

func NewUserRepository(db *gorm.DB) *UserRepository {
	return &UserRepository{
		Db: db,
	}
}

func (ur UserRepository) Browse() ([]models.Model, error) {
	log.Println("repositories:UserRepository - Browse")
	users := []models.User{}
	// fetch data
	errs := ur.Db.Find(&users).GetErrors()
	if len(errs) > 0 {
		log.Println("error occured:", errs[0])
		return nil, errs[0]
	}
	result := make([]models.Model, len(users))
	for i, v := range users {
		result[i] = v
	}
	log.Println("users: ", users)
	return result, nil
}

func (ur UserRepository) Read(id uint) (models.Model, error) {
	log.Println("repositories:UserRepository - Read")
	var user models.User
	errs := ur.Db.Where("id = ?", id).First(&user).GetErrors()
	if len(errs) > 0 {
		log.Println("error occured:", errs[0])
		return nil, errs[0]
	}
	return user, nil
}

func (ur UserRepository) Add(payload models.Model) (models.Model, error) {
	log.Println("repositories:UserRepository - Create")
	user, _ := payload.(models.User)
	errs := ur.Db.Create(&user).GetErrors()
	if len(errs) > 0 {
		log.Println("error occured:", errs[0])
		return nil, errs[0]
	}
	return user, nil
}

// extra implementation
func (ur UserRepository) ReadByUsername(username string) (models.Model, error) {
	log.Println("repositories:UserRepository - ReadByUsername")
	var user models.User
	errs := ur.Db.Where("username = ?", username).First(&user).GetErrors()
	if len(errs) > 0 {
		log.Println("error occured:", errs[0])
		return nil, errs[0]
	}
	return user, nil
}
