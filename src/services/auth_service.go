package services

import (
	"errors"
	"log"
	"strings"

	"gitlab.com/andy-shi88/my-family/src/models"
	customModel "gitlab.com/andy-shi88/my-family/src/models/custom_model"
	jwtModel "gitlab.com/andy-shi88/my-family/src/models/jwt_model"
	"gitlab.com/andy-shi88/my-family/src/repositories"
	crypto "gitlab.com/andy-shi88/my-family/src/utils/crypto"
	stringUtils "gitlab.com/andy-shi88/my-family/src/utils/string"
	"golang.org/x/crypto/bcrypt"
)

type AuthService struct {
	Repository      *repositories.UserRepository
	TokenRepository *repositories.TokenRepository
}

// user registration by passing `username`, `password`
// password will be salted by random string and encrypted using bcrypt
// error = nil if no error occured
func (auth AuthService) Register(payload interface{}) (interface{}, error) {
	log.Println("services:AuthService - Register:", payload)
	user, ok := payload.(models.User)
	if !ok {
		log.Fatal("server fail to parse user data")
		return nil, errors.New("wrong user payload")
	}
	log.Println("generating salt...")
	salt := stringUtils.GetRandomString(64)
	saltedPassword := user.Password + salt
	log.Println("hashing password...")
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(saltedPassword), 8)
	if err != nil {
		return nil, errors.New("server fail to hash user password")
	}
	registerUser := models.User{
		Username: user.Username,
		Password: string(hashedPassword),
		Salt:     salt,
	}
	result, err := auth.Repository.Add(registerUser)
	if err != nil {
		if strings.Contains(err.Error(), "Error 1062: Duplicate entry") {
			return nil, errors.New("user with username " + user.Username + " already exist")
		}
		return nil, errors.New("server fail to save user to database")
	}
	return result, nil
}

// user login by providing `username` and `password` through payload
// return LoginResponse and error = nil if no error occurred
func (auth AuthService) Login(payload interface{}) (interface{}, error) {
	log.Println("services:AuthService - login", payload)
	user, ok := payload.(models.User)
	if !ok {
		log.Fatal("server fail to parse user data")
		return nil, errors.New("wrong user payload")
	}
	result, err := auth.Repository.ReadByUsername(user.Username)
	if err != nil {
		log.Println(err.Error())
		return nil, errors.New(err.Error())
	}
	parsedResult, _ := result.(models.User)

	err = bcrypt.CompareHashAndPassword([]byte(parsedResult.Password), []byte(user.Password+parsedResult.Salt))
	if err != nil {
		log.Println(err.Error())
		return nil, errors.New("wrong username or password submitted")
	}
	token, tokenErr := crypto.GenerateJWT(jwtModel.JWTPayload{Username: parsedResult.Username, ID: parsedResult.ID})
	if tokenErr != nil {
		log.Println(tokenErr.Error())
		return nil, tokenErr
	}
	refreshToken := stringUtils.GetRandomString(64)

	return customModel.LoginResponse{Token: token, RefreshToken: refreshToken}, nil
}

// get new access token by providing refresh token
// err will be nil if everything goes well
func (auth AuthService) RefreshToken(payload interface{}) (interface{}, error) {
	log.Println("services:AuthService - refreshToken", payload)
	refresh, ok := payload.(customModel.RefreshRequest)
	if !ok {
		log.Fatal("server fail to parse refresh token data")
		return nil, errors.New("wrong refresh token payload")
	}
	result, err := auth.TokenRepository.ReadByRefreshToken(refresh.RefreshToken)
	if err != nil {
		log.Println(err.Error())
		return nil, errors.New(err.Error())
	}
	tokenUser, _ := result.(models.TokenUser)
	token, tokenErr := crypto.GenerateJWT(jwtModel.JWTPayload{Username: tokenUser.Username, ID: tokenUser.UserId})
	if tokenErr != nil {
		log.Println(tokenErr.Error())
		return nil, tokenErr
	}
	return customModel.LoginResponse{Token: token, RefreshToken: stringUtils.GetRandomString(64)}, nil
}
