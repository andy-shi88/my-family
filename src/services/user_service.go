package services

import (
	"gitlab.com/andy-shi88/my-family/src/repositories"
)

type UserService struct {
	Repository *repositories.UserRepository
}

// get user list
// error = nil if no error occured
func (us UserService) GetUserList() (interface{}, error) {
	users, err := us.Repository.Browse()
	if err != nil {
		return nil, err
	}
	return users, nil
}

// get user by their id
// error = nil if no error occured
func (us UserService) GetUser(id uint) (interface{}, error) {
	user, err := us.Repository.Read(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}
