package api

type ResponseBuilder struct {
	response Response
}

type Response struct {
	Data interface{} `json:"data"`
	Meta Meta        `json:"meta"`
}

type Meta struct {
	ErrorCode    string `json:"error_code"`
	ErrorMessage string `json:"error_message"`
	Message      string `json:"message"`
	Success      bool   `json:"successs"`
}

func (r ResponseBuilder) SetData(data interface{}) ResponseBuilder {
	r.response.Data = data
	return r
}

func (r ResponseBuilder) SetErrorCode(errorCode string) ResponseBuilder {
	r.response.Meta.ErrorCode = errorCode
	return r
}

func (r ResponseBuilder) SetErrorMessage(errorMessage string) ResponseBuilder {
	r.response.Meta.ErrorMessage = errorMessage
	return r
}

func (r ResponseBuilder) SetMessage(message string) ResponseBuilder {
	r.response.Meta.Message = message
	return r
}

func (r ResponseBuilder) SetSuccess(success bool) ResponseBuilder {
	r.response.Meta.Success = success
	return r
}

func (r ResponseBuilder) Build() Response {
	return r.response
}
