package api

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type MockData struct {
	Name string
	Age  uint
}

// success response builder assertion test
func TestSuccessResponseBuilder(t *testing.T) {
	mockSuccessData := MockData{Name: "TestName", Age: 10}
	mockSuccessMessage := "test succeded"
	successResponse := ResponseBuilder{}.SetData(mockSuccessData).SetMessage(mockSuccessMessage).SetSuccess(true).Build()

	assert.Equal(t,
		Response{
			Data: mockSuccessData,
			Meta: Meta{
				ErrorCode:    "",
				ErrorMessage: "",
				Message:      mockSuccessMessage,
				Success:      true,
			},
		}, successResponse)

}

// fail response builder assertion test
func TestFailResponseBuilder(t *testing.T) {
	mockFailMessage := "test failed"
	errorCode := "E422"
	errorMessage := "Invalid payload type"
	failResponse := ResponseBuilder{}.SetData(nil).SetMessage(mockFailMessage).SetErrorCode(errorCode).SetErrorMessage(errorMessage).SetSuccess(false).Build()
	assert.Equal(t,
		Response{
			Data: nil,
			Meta: Meta{
				ErrorCode:    errorCode,
				ErrorMessage: errorMessage,
				Message:      mockFailMessage,
				Success:      false,
			},
		}, failResponse)
}
