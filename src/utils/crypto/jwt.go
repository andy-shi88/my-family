package crypto

import (
	"errors"
	"os"

	jwt "github.com/dgrijalva/jwt-go"
)

func GenerateJWT(payload interface{}) (string, error) {
	sign := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), jwt.MapClaims{
		"payload": payload,
		"exp":     os.Getenv("JWT_EXP"),
	})
	token, err := sign.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return "", err
	}
	return token, nil
}

func VerifyJWT(tokenString string) (interface{}, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("Unexpected signing method")
		}
		return []byte(os.Getenv("JWT_SECRET")), nil
	})
	if err != nil {

		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				return nil, errors.New("malformed token submitted")
			} else if ve.Errors&(jwt.ValidationErrorExpired) != 0 {
				return nil, errors.New("token has expired")
			}
			return nil, errors.New("error Cannot Identify Token")
		}
	}
	if token.Valid {

		return token.Claims, nil
	}
	return nil, errors.New("error Cannot Identify Token")
}
