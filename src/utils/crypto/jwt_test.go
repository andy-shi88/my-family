package crypto

import (
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
)

type MockPayload struct {
	Name string
	Age  uint
}

func TestGenerateJwtSuccess(t *testing.T) {
	result, err := GenerateJWT(MockPayload{Name: "Andy", Age: 10})
	assert.IsType(t, "", result, "token should be of type string")
	assert.Equal(t, nil, err)
}

func TestVerifyJwtSuccess(t *testing.T) {
	payload := make(map[string]interface{})
	payload["name"] = "andy"
	payload["email"] = "andy@mail.com"
	result, _ := GenerateJWT(payload)
	verifiedPayload, _ := VerifyJWT(result)
	p, _ := verifiedPayload.(jwt.MapClaims)["payload"]

	assert.Equal(t, payload, p)
}
