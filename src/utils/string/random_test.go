package string

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateRandomString(t *testing.T) {
	for i := 0; i < 50; i++ {
		result := GetRandomString(i)
		assert.IsType(t, "", result, "result of method should be type of string")
		assert.Len(t, result, i, "result length should be the same as i value")
	}
}
